﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public GameObject Indicator;
    public GameObject Line;

    public OVRInput.Controller Controller;

    public LayerMask HitLayer;

    public CharacterController Player;

    private static Teleport _sTeleporting = null;

    private GameObject indicatorClone = null;
    private Transform Position;
    private Transform Orientation;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, Controller).y > 0.5f)
        {
            if (_sTeleporting == null)
                _sTeleporting = this;
        }
        else
        {
            if (_sTeleporting == this)
                _sTeleporting = null;
        }

        if (_sTeleporting == this)
        {
            Line.SetActive(true);

            if (Physics.Raycast(Line.transform.position, Line.transform.forward,
                out var hit, 100.0f, HitLayer))
            {
                Vector3 hitPoint = hit.point;
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
                {
                    if (indicatorClone == null)
                    {
                        indicatorClone = Instantiate(Indicator);
                        Position = indicatorClone.transform.Find("Position");
                        Orientation = Position.transform.Find("Orientation");
                    }
                    Line.GetComponent<LineRenderer>().startColor = Color.green;
                    Line.GetComponent<LineRenderer>().endColor = Color.green;
                }
                else
                {
                    if (indicatorClone != null)
                    {
                        Destroy(indicatorClone);
                    }

                    Line.GetComponent<LineRenderer>().startColor = Color.red;
                    Line.GetComponent<LineRenderer>().endColor = Color.red;
                }

                if (indicatorClone != null)
                {
                    indicatorClone.transform.position = hitPoint;
                    Position.up = hit.normal;
                    Orientation.forward = Player.transform.forward;
                    Orientation.localEulerAngles = new Vector3(0.0f, Orientation.localEulerAngles.y, 0.0f);
                }
            }


            
        }
        else
        {
            Line.SetActive(false);

            if (indicatorClone != null)
            {
                Player.enabled = false;
                Player.transform.position = indicatorClone.transform.position + Vector3.up * Player.height / 2.0f;
                Player.enabled = true;
                
                if (Physics.Raycast(Line.transform.position, Line.transform.forward,
                    out var hit2, 100.0f, HitLayer, QueryTriggerInteraction.Collide))
                {
                    if (hit2.collider.gameObject.layer == LayerMask.NameToLayer("ButterflyTrigger"))
                    {
                        ButterflySpawner.Instance.Spawn();
                    }
                }
                
                Destroy(indicatorClone);
            }
        }
    }
}