﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Butterfly : MonoBehaviour
{
    public ButterflyGroup Group;
    public float LiveTime;
    public Vector3 Direction;
    public float Speed;

    private float timeSinceSpawn = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        timeSinceSpawn = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceSpawn += Time.deltaTime;

        if (timeSinceSpawn > LiveTime)
        {
            transform.localScale *= 0.95f;

            if (transform.localScale.x < 0.05f)
            {
                Destroy(gameObject);
            }
        }

        transform.forward = Direction;
        transform.position += Speed * Direction;
    }
}
