﻿using System;
using System.Collections;
using System.Collections.Generic;
using Oculus.Platform.Samples.VrHoops;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class Movement : MonoBehaviour
{
    public OVRPlayerController PlayerController;

    [Serializable]
    private struct DataPoint
    {
        public float TimeStamp;
        public float LVelocity;
        public float RVelocity;
        public float LdotR;
    }

    bool NotMoving(DataPoint data)
    {
        return data.LdotR > -0.1f;
    }

    bool Moving(DataPoint data)
    {
        return data.LdotR < 1.0f;
    }

    private struct State
    {
        public bool isState;
        public float stateStartTime;
        public float stateEndTime;

        public void SetState(bool value)
        {
            if (value)
            {
                if (isState) return;
                isState = true;
                stateStartTime = Time.fixedTime;
                stateEndTime = Time.fixedTime;
            }
            else
            {
                if (!isState) return;
                isState = false;
                stateEndTime = Time.fixedTime;
            }
        }
    }

    private State notMoving;
    private State moving;

    private bool isMoving;
    //
    // private List<DataPoint> data;
    // private List<bool> isMovingData;
    // private List<bool> realMovingData;

    private DataPoint frameData;
    private DataPoint lastFrameData;

    // Start is called before the first frame update
    void Start()
    {
        // data = new List<DataPoint>();
        // isMovingData = new List<bool>();
        // realMovingData = new List<bool>();

        notMoving.isState = false;
        notMoving.stateStartTime = 0.0f;
        notMoving.stateEndTime = 0.0f;

        moving.isState = false;
        moving.stateStartTime = 0.0f;
        moving.stateEndTime = 0.0f;

        isMoving = false;
    }


    private bool goingDown = false;
    private DataPoint minData;
    private DataPoint maxData;
    private float magnitude = 0.0f;
    private float frequency = 1.0f;
    public bool GoingDown
    {
        get => goingDown;
        set
        {
            if (goingDown == value) return;
            if (value)
            {
                minData = frameData;
            }
            else
            {
                maxData = frameData;
            }

            magnitude = maxData.LdotR - minData.LdotR;
            frequency = 1 / Mathf.Abs(maxData.TimeStamp - minData.TimeStamp);

            goingDown = value;
        }
    }

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Start))
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        if (OVRInput.GetDown(OVRInput.Button.Four, OVRInput.Controller.Touch))
        {
            UnityEngine.XR.InputTracking.Recenter();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 lVelocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch);
        Vector3 rVelocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);
        float time = Time.fixedTime;

        frameData = new DataPoint
        {
            TimeStamp = time,
            LVelocity = lVelocity.magnitude,
            RVelocity = rVelocity.magnitude,
            LdotR = Vector3.Dot(lVelocity, rVelocity),
        };

        notMoving.SetState(NotMoving(frameData));
        moving.SetState(Moving(frameData));


        isMoving = !(notMoving.isState && (Time.fixedTime - notMoving.stateStartTime > 0.5f) ||
                     (!notMoving.isState && !moving.isState)) &&
                   (moving.isState || (!moving.isState && Time.fixedTime - moving.stateEndTime < 0.25f));

        // data.Add(frameData);
        // isMovingData.Add(isMoving);
        // realMovingData.Add(OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.Touch));


        if (isMoving)
        {
            //PlayerController.MoveForwardSpeed = 4.0f / moving.stateRepeatTime;
            if (Mathf.Abs(frameData.LdotR - lastFrameData.LdotR) > 0.15f)
                GoingDown = frameData.LdotR < lastFrameData.LdotR;
            float targetSpeed = Mathf.Clamp(Mathf.Pow(frequency / 10.0f, 2.0f) * 2, 0.0f, 2.0f);
            PlayerController.MoveForwardSpeed = Mathf.Lerp(PlayerController.MoveForwardSpeed, targetSpeed, Time.fixedDeltaTime / 2.0f);

        }
        else
        {
            PlayerController.MoveForwardSpeed = 0.0f;
        }

        lastFrameData = frameData;
    }

    // private void OnDestroy()
    // {
    //     string str = "TimeStamp,LeftV,RightV,LdR,isMoving,realMoving\n";
    //     for (var index = 0; index < data.Count; index++)
    //     {
    //         var d = data[index];
    //         str +=
    //             $"{d.TimeStamp},{d.LVelocity},{d.RVelocity},{d.LdotR},{(isMovingData[index] ? 1 : 0)},{(realMovingData[index] ? 1 : 0)}\n";
    //     }
    //
    //     System.IO.File.WriteAllText(@"C:\Users\shuru\Desktop\data.csv", str);
    // }
}