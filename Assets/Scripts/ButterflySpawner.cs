﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class ButterflySpawner : MonoBehaviour
{
    public GameObject GroupPrefab;

    public Transform StartCube;
    public Transform EndCube;

    public static ButterflySpawner Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Spawn()
    {
        Vector3 startPoint =
            new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f));
        Vector3 endPoint =
            new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f));

        bool leftToRight = Random.Range(0, 2) == 0;

        GameObject newGroup = Instantiate(GroupPrefab);

        StartCoroutine(GroupMovement(newGroup, startPoint, endPoint, leftToRight));
    }

    IEnumerator GroupMovement(GameObject group, Vector3 startPoint, Vector3 endPoint, bool leftToRight)
    {
        Transform sT;
        Transform eT;
        if (leftToRight)
        {
            sT = StartCube;
            eT = EndCube;
        }
        else
        {
            sT = EndCube;
            eT = StartCube;
        }

        Vector3 sP = sT.TransformPoint(startPoint);
        Vector3 eP = eT.TransformPoint(endPoint);

        float t = 0.0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime / 5.0f;

            group.transform.position = Vector3.Lerp(sP, eP, t);

            yield return null;
        }
        
        Destroy(group);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        if (other.gameObject.layer == LayerMask.NameToLayer("ButterflyTrigger"))
        {
            Spawn();
            Destroy(other.gameObject);
        }
    }
}