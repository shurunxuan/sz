﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButterflyGroup : MonoBehaviour
{
    public List<GameObject> Butterflies;

    public float GenerateProbability = 0.5f;

    public int MaxButterflyNumber = 15;

    private List<GameObject> CurrentButterflies;

    // Start is called before the first frame update
    void Start()
    {
        CurrentButterflies = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        CurrentButterflies.RemoveAll(go => go == null);

        if (CurrentButterflies.Count < MaxButterflyNumber)
        {
            if (Random.Range(0.0f, 1.0f) < GenerateProbability)
            {
                GameObject newButterfly = Instantiate(Butterflies[Random.Range(0, Butterflies.Count - 1)],
                    transform.position, transform.rotation, null);
                
                newButterfly.transform.localScale = Vector3.one * 0.2f;

                Butterfly b = newButterfly.AddComponent<Butterfly>();
                b.Group = this;
                b.Direction = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                b.Direction.Normalize();
                b.Speed = Random.Range(0.005f, 0.015f);
                b.LiveTime = Random.Range(0.5f, 1.5f);
                
                CurrentButterflies.Add(newButterfly);
            }
        }
    }
}