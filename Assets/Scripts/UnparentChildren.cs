﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnparentChildren : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // var fg = transform.GetComponentsInChildren<FakeGrabbable>();
        // foreach (var f in fg)
        // {
        //     f.gameObject.transform.SetParent(null, true);
        // }
        transform.DetachChildren();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
