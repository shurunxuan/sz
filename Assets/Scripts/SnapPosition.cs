﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SnapPosition : MonoBehaviour
{
    private MeshRenderer renderer;

    private List<FakeGrabbable> grabbables;

    private HashSet<FakeGrabbable> collidingWith;

    public FakeGrabbable OccupiedBy
    {
        get
        {
            foreach (var fg in collidingWith)
            {
                if (fg != null && grabbables.Contains(fg))
                {
                    return fg;
                }
            }

            return null;
        }
    }

    FakeGrabbable GetFakeGrabbableFromCollider(Collider c)
    {
        if (c.gameObject.layer != LayerMask.NameToLayer("Puzzle") &&
            c.gameObject.layer != LayerMask.NameToLayer("IgnorePuzzle"))
            return null;

        if (c.transform.childCount == 0) return null;

        Transform child = c.transform.GetChild(0);
        FakeGrabbable result = child.GetComponent<FakeGrabbable>();
        return result;
    }

    // Start is called before the first frame update
    void Start()
    {
        collidingWith = new HashSet<FakeGrabbable>();
        renderer = GetComponent<MeshRenderer>();
        grabbables = FakeGrabbable.AllFakeGrabbles.Where(
            fakeGrabbable => fakeGrabbable.AllowedTransforms.Contains(this.gameObject)).ToList();
    }

    // Update is called once per frame
    void Update()
    {
        bool shouldShow = grabbables.Any(g => g.IsGrabbed);
        renderer.enabled = shouldShow && OccupiedBy == null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (OccupiedBy != null)
            if (OccupiedBy.transform.parent.gameObject != other.transform.gameObject)
                return;
        FakeGrabbable fg = GetFakeGrabbableFromCollider(other);
        if (fg != null && grabbables.Contains(fg))
        {
            fg.SnappingTo = transform;
            if (fg.SnappingTo == transform)
            {
                collidingWith.Add(fg);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        FakeGrabbable fg = GetFakeGrabbableFromCollider(other);
        if (fg != null && grabbables.Contains(fg))
        {
            if (collidingWith.Contains(fg))
            {
                fg.SnappingTo = null;
                collidingWith.Remove(fg);
            }
        }
    }
}