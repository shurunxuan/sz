﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataRecording : MonoBehaviour
{
    private struct DataPoint
    {
        public float TimeSinceStart;
        public Vector3 LeftHandPosition;
        public Vector3 RightHandPosition;
        public Vector3 PlayerPosition;
        public float TotalDistance;
    }

    private List<DataPoint> data;

    public Transform LeftHandAnchor;
    public Transform RightHandAnchor;
    public Transform Player;
    // Start is called before the first frame update
    void Start()
    {
        data = new List<DataPoint>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        DataPoint d = new DataPoint
        {
            TimeSinceStart = Time.unscaledTime,
            LeftHandPosition = Player.InverseTransformPoint(LeftHandAnchor.position),
            RightHandPosition = Player.InverseTransformPoint(RightHandAnchor.position),
            PlayerPosition = Player.position
        };
        if (data.Count == 0) d.TotalDistance = 0.0f;
        else
        {
            DataPoint lastData = data[data.Count - 1];
            d.TotalDistance = Vector3.Distance(d.PlayerPosition, lastData.PlayerPosition) + lastData.TotalDistance;
        }

        data.Add(d);
    }

    private void OnDestroy()
    {
        List<string> lines = new List<string>(data.Count + 2);
        lines.Add(",LeftHandPosition,,,RightHandPosition,,,PlayerPosition,,,TotalDistance");
        lines.Add("Time,x,y,z,x,y,z,x,y,z,TotalDistance");
        for (var index = 0; index < data.Count; index++)
        {
            var d = data[index];
            lines.Add(
                $"{d.TimeSinceStart},{d.LeftHandPosition.x},{d.LeftHandPosition.y},{d.LeftHandPosition.z},{d.RightHandPosition.x},{d.RightHandPosition.y},{d.RightHandPosition.z},{d.PlayerPosition.x},{d.PlayerPosition.y},{d.PlayerPosition.z},{d.TotalDistance}");
        }

        System.IO.File.WriteAllLines($@"{DateTime.Now.ToString("HH_mm_ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)}.csv", lines);
    }
}
