﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera MainCamera;

    public float Height;

    public float MoveSpeed;

    public float RotateSpeed;

    public Transform EyeDirectionIndicator;

    private float resetViewValue;
    private float lastResetViewValue;

    public Transform LeftController;
    public Transform RightController;

    void Awake()
    {
        if (MainCamera == null)
        {
            MainCamera = Camera.main;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        resetViewValue = 0;
        lastResetViewValue = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 currentPos = transform.position;
        currentPos.y = Height;

        Vector3 forward = MainCamera.transform.forward;

        Vector3 realRight = Vector3.Normalize(Vector3.Cross(Vector3.up, forward));
        Vector3 realForward = Vector3.Normalize(Vector3.Cross(realRight, Vector3.up));

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        float rightHorizontal = Input.GetAxis("RightHorizontal");

        Vector3 moveDirection = moveHorizontal * realRight + moveVertical * realForward;

        resetViewValue = Input.GetAxis("ResetView");

        if (resetViewValue > 0.5f && lastResetViewValue <= 0.5f)
        {
            transform.forward = realForward;
        }

        lastResetViewValue = resetViewValue;

        transform.position = currentPos + moveDirection * MoveSpeed * Time.deltaTime;
        transform.rotation *= Quaternion.AngleAxis(RotateSpeed * Time.deltaTime * rightHorizontal, Vector3.up);



        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(MainCamera.transform.position, MainCamera.transform.forward, out hit, 100.0f))
        {
            EyeDirectionIndicator.position = hit.point;
        }

        LeftController.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        LeftController.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);
        RightController.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
        RightController.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);

        MainCamera.transform.localPosition = OVRPlugin.GetNodePose(OVRPlugin.Node.Head, OVRPlugin.Step.Render).ToOVRPose().position;
    }
}
