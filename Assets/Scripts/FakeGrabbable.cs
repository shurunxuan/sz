﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FakeGrabbable : MonoBehaviour
{
    public static List<FakeGrabbable> AllFakeGrabbles;
    private GameObject clonnedSelf;

    public List<GameObject> AllowedTransforms;

    private Transform isSnapping = null;
    private OVRGrabbable realGrabbable = null;

    private bool lastGrabbed = false;

    public Transform SnappingTo
    {
        get => isSnapping;
        set
        {
            if (value == isSnapping) return;
            if (value != null && isSnapping != null) return;
            isSnapping = value;

            StartCoroutine(value == null ? ExitSnapTransition() : SnapTransition());
        }
    }

    private bool isTransitioning = false;

    public bool IsGrabbed => realGrabbable.isGrabbed;

    IEnumerator SnapTransition()
    {
        foreach (var ag in ArrowGroup.Instances)
        {
            if (ag.name.Contains("Puzzle"))
            {
                ag.ShouldStop = true;
            }
        }
        isTransitioning = true;
        transform.SetParent(null, true);
        while (true)
        {
            if (isSnapping == null)
            {
                StartCoroutine(ExitSnapTransition());
                break;
            }

            if (!(Vector3.Distance(isSnapping.position, transform.position) > 0.001f ||
                  Quaternion.Angle(isSnapping.rotation, transform.rotation) > 0.001f)) break;
            if (!IsGrabbed) break;
            transform.position = Vector3.Lerp(transform.position, isSnapping.position, 20.0f * Time.deltaTime);
            transform.rotation = Quaternion.Slerp(transform.rotation, isSnapping.rotation, 20.0f * Time.deltaTime);


            yield return null;
        }

        transform.SetParent(clonnedSelf.transform, true);
        isTransitioning = false;
    }

    IEnumerator ExitSnapTransition()
    {
        isTransitioning = true;

        while (Vector3.Distance(transform.localPosition, Vector3.zero) > 0.001f ||
               Quaternion.Angle(transform.localRotation, Quaternion.identity) > 0.001f)
        {
            if (!IsGrabbed) break;
            if (isSnapping != null) break;
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, 20.0f * Time.deltaTime);
            transform.localRotation =
                Quaternion.Slerp(transform.localRotation, Quaternion.identity, 20.0f * Time.deltaTime);

            yield return null;
        }

        isTransitioning = false;
    }

    private void Awake()
    {
        if (AllFakeGrabbles == null) AllFakeGrabbles = new List<FakeGrabbable>();

        AllFakeGrabbles.Add(this);
    }

    private void OnDestroy()
    {
        AllFakeGrabbles.Remove(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        clonnedSelf = Instantiate(gameObject, gameObject.transform);
        clonnedSelf.transform.SetParent(null, true);
        clonnedSelf.transform.position = transform.position;
        clonnedSelf.transform.rotation = transform.rotation;
        clonnedSelf.transform.localScale = transform.lossyScale;
        DestroyImmediate(clonnedSelf.GetComponent<FakeGrabbable>());
        DestroyImmediate(GetComponent<OVRGrabbable>());
        DestroyImmediate(GetComponent<Collider>());
        DestroyImmediate(GetComponent<Rigidbody>());
        clonnedSelf.GetComponent<MeshRenderer>().enabled = false;
        realGrabbable = clonnedSelf.GetComponent<OVRGrabbable>();

        transform.SetParent(clonnedSelf.transform, true);
    }

    // Update is called once per frame
    void Update()
    {
        if (!realGrabbable.isGrabbed && lastGrabbed && isSnapping != null)
        {
            clonnedSelf.transform.position = transform.position;
            clonnedSelf.transform.rotation = transform.rotation;

            SnappingTo = null;
        }

        clonnedSelf.layer = LayerMask.NameToLayer(IsGrabbed ? "IgnorePuzzle" : "Puzzle");
        gameObject.layer = clonnedSelf.layer;

        lastGrabbed = realGrabbable.isGrabbed;

        if (isTransitioning) return;

        if (isSnapping != null && IsGrabbed)
        {
            transform.position = isSnapping.position;
            transform.rotation = isSnapping.rotation;
        }
        else
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
}