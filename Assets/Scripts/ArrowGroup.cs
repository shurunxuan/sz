﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArrowGroup : MonoBehaviour
{
    public static List<ArrowGroup> Instances;
    public List<GameObject> Arrows;

    private List<MeshRenderer> renderers;

    private static readonly int ColorPropertyID = Shader.PropertyToID("_Color");

    public float lerpTime;
    public float delayTime;
    public float litTime;
    public float disappearTime;

    public Color color;

    public bool ShouldStop = false;

    private void Awake()
    {
        if (Instances == null) Instances = new List<ArrowGroup>();
        
        Instances.Add(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        renderers = Arrows.Select(go => go.GetComponent<MeshRenderer>()).ToList();

        foreach (var r in renderers)
        {
            MaterialPropertyBlock pb = new MaterialPropertyBlock();
            r.GetPropertyBlock(pb);
            Color c = color;
            c.a = 0.0f;
            pb.SetColor(ColorPropertyID, c);
            r.SetPropertyBlock(pb);
        }

        StartCoroutine(StartAnimation());
    }

    // Update is called once per frame
    void Update()
    {
        Instances.RemoveAll(ag => ag == null);
    }

    IEnumerator StartAnimation()
    {
        yield return new WaitForSeconds(3.0f);
        
        for (var i = 0; i < renderers.Count; i++)
        {
            StartCoroutine(BlinkArrow(renderers[i], delayTime * (i + 1)));
        }
    }

    IEnumerator LerpAlpha(MeshRenderer r, float start, float end)
    {
        float t = 0.0f;
        MaterialPropertyBlock pb = new MaterialPropertyBlock();
        r.GetPropertyBlock(pb);
        Color c = color;
        while (t <= 1.0f)
        {
            t += Time.deltaTime / lerpTime;
            c.a = Mathf.Lerp(start, end, t);
            pb.SetColor(ColorPropertyID, c);
            r.SetPropertyBlock(pb);
            yield return null;
        }
    }

    IEnumerator BlinkArrow(MeshRenderer r, float eachDelayTime)
    {
        yield return new WaitForSeconds(eachDelayTime);
        while (!ShouldStop)
        {
            yield return StartCoroutine(LerpAlpha(r, 0.0f, 1.0f));
            yield return new WaitForSeconds(litTime);
            yield return StartCoroutine(LerpAlpha(r, 1.0f, 0.0f));
            yield return new WaitForSeconds(disappearTime);
        }
    }
}
